const nodemailer = require('nodemailer') ;

const transporter = nodemailer.createTransport({
 service : 'gmail',
 auth : {
 utilisateur : 'votre_email@gmail.com',
 passe : 'votre_mot_de_passe',
 },
});

Options du message
const mailOptions = {
 de : 'votre_email@gmail.com',
 à : 'email_destinataire@example.com',
 subject : 'Test e-mail avec Node.js',
 text : 'Exemple de mail envoyé depuis Node.js.',
};

transporter.sendMail(mailOptions, function(error, info){
 if (erreur) {
 console.error('Erreur lors de l\'envoi de l\'e-mail :', error) ;
 } else {
 console.log('E-mail envoyé avec succès :', info.response) ;
 }
});